# Lista de Orçamento Concluidos por Rui B. Siqueira

| Licitação  | Nº Orçamento | Total | Conclusão | Objeto (Resumido) |
| :-- | :--: | --: | :--: | :-- |
| 2023-001  | Cesta de Preços 18679/000197 | R$ 101.828,74 | 13/02/2023 | Material de Expediente (Administração) |
| 2023-001B | Cesta de Preços 19152/000197 | R$  88.305,51 | 27/02/2023 | Material de Expediente (Ação Social) |
| 2023-001C | Cesta de Preços 19463/000197 | R$  91.664,09 | 09/03/2023 | Material de Expediente (Saúde) |
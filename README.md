![logotipo]
## DOCUMENTOS 2023 DA PREFEITURA MUNICIPAL DE INGAZEIRA-PE

Esta pasta é uma armazenamento em nuvem de no máximo 2GB reservado exclusivamente para documentos. Como o espaço é limitado e curto, favor não inserir vídeos e fotos nesta nuvem.

* **Repositório**: [2021], [2022], **[2023]**, e  [Geral]
* **Email**: [ruibritodesiqueira@disroot.org]   
* **Chat**: [is.gd/IngaWebChat]

[ruibritodesiqueira@disroot.org]:mailto:ruibritodesiqueira@disroot.org
[is.gd/IngaWebChat]:https://is.gd/IngaWebChat
[Geral]:https://gitlab.com/prefeitura-de-ingazeira-pe/
[2021]:https://gitlab.com/prefeitura-de-ingazeira-pe/documentos-2021
[2022]:https://gitlab.com/prefeitura-de-ingazeira-pe/documentos-2022
[2023]:https://gitlab.com/prefeitura-de-ingazeira-pe/documentos-2023
[logotipo]:https://ingazeira.pe.gov.br/site/wp-content/uploads/2022/01/logo-certa-2.png